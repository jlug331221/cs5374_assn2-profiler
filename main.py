import os, sys
from profiler import Profiler
import test_suite as ts

source_file = os.path.join(os.getcwd(), 'source.py')

'''
    # create Profiler obj
    # add line numbers to source file
    
    # set a trace on source file
    # run a test suite
    # remove trace on source file
    
    # report test coverage results
  '''
def main():
  profiler = Profiler(source_file)
  profiler.add_line_numbers_to_source_file()

  sys.settrace(profiler.trace)
  ts.exec_tests()
  sys.settrace(None)

  profiler.report_results()

if __name__ == '__main__':
  main()