def is_even(num):
  if(num % 2 == 0):
    return True

  return False

def is_odd(num):
  if(num % 2 != 0):
    return True

  return False

def foo(arg):
  print(arg)

def procedure_with_loop():
  for i in range(5):
    i = i - 1

def recursive_procedure(arg):
  if arg == 0:
    return

  recursive_procedure(arg - 1)