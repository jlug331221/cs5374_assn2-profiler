from source import *

def test_is_even_number():
  assert is_even(2) == True
  assert is_even(3) == False

def test_is_odd_number():
  assert is_odd(2) == False
  assert is_odd(3) == True

def test_foo():
  foo('Justin')

def test_procedure_with_loop():
  procedure_with_loop()

def test_recursive_procedure():
  recursive_procedure(2)

def exec_tests():
  test_is_even_number()
  test_is_odd_number()
  test_foo()
  procedure_with_loop()
  test_recursive_procedure()