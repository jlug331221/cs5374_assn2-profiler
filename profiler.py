import inspect

class Profiler(object):
  def __init__(self, source_file_to_watch):
    self.source_file = source_file_to_watch
    self.source_code = open(self.source_file).readlines()
    self.lines_covered = []
    self.tested_function_names = []

  def trace(self, frame, event, arg):
    current_file = inspect.getframeinfo(frame).filename
    function_name = inspect.getframeinfo(frame).function

    if self.source_file in current_file and (event == 'line' or event == 'call'):
      self.lines_covered.append(frame.f_lineno)

      if event == 'call':
        if function_name not in self.tested_function_names:
          self.tested_function_names.append(function_name)

    return self.trace

  def report_results(self):
    print('Test cases for \'' + self.source_file + '\', covers lines:')
    for line_covered in self.lines_covered:
      print(str(line_covered) + ' ', end='')

    print('\n\nTest suite covered functions:')
    for function_name in self.tested_function_names:
      print(function_name)

  def add_line_numbers_to_source_file(self):
    with open("source_file_with_line_numbers.txt", "w") as file_with_line_numbers:
      for i, line in enumerate(self.source_code):
        file_with_line_numbers.write(str(i+1) + '\t ' + line)